<?php

return [
    /*
        Array of middleware groups to handle the internal POST requests.
        Use middleware groups that do not include VerifyCsrfToken, or at least
        add the internal route ( job/execute ) within the exceptions.
    */
    'middleware' => [],
];
