# Laravel Loopback Queue

This is a dummy driver for [Laravel's queue subsystem](https://laravel.com/docs/queues).

When you schedule a job through this queue, an HTTP request is sent to the application itself (on a dedicated route) and it will not wait for a response, so to let the job to be executed as a different detached state while you continue elaboration or even return a response to the user.

Useful to have actual asyncronous jobs without having to deal with worker processes, dedicated services, complex environments or whatever, but absolutely not reccomended for intensive scale.

## Installation

```
composer require madbob/laravel-queue-loopback
```

In your .env file, set:

```
QUEUE_DRIVER=loopback
```

In your `config/queue.php`, add the following block among available connections:

```
'loopback' => [
    'driver' => 'loopback',

    /*
        The key is intended to protect the route used for internal execution, as
        it is still a public route and it is unprotected by the `auth`
        middleware or other native authentication methods.
    */
    'key' => 'put_here_a_random_key',
],
```

For advanced configuration you can execute `php artisan vendor:publish --tag=config` to obtain a new file in `config/loopback-queue.php`, where you can specify:

- the middleware group to be used for the internal routing. By default it is an empty array (no middleware group); if you intend to specify one or more groups it is raccomended to define a new one not including the native `VerifyCsrfToken` middleware, as internal POSTs used to trigger the jobs would fail due the missing CSRF token

## Usage

It works exactly as any other queue in Laravel: create [your own Jobs](https://laravel.com/docs/queues#generating-job-classes) and [dispatch them](https://laravel.com/docs/queues#dispatching-jobs) as usual.

But, not as usual, you have not to execute any `php artisan queue:work` command or similar.

The loopback driver does not handles different queues nor priorities.
