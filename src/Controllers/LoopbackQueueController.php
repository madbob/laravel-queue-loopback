<?php

namespace MadBob\LaravelQueue\Loopback\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use MadBob\LaravelQueue\Loopback\LoopbackJob;

class LoopbackQueueController extends Controller
{
    public function execute(Request $request)
    {
        $job = new LoopbackJob(app(), $request->getContent());
        $job->fire();
    }
}
