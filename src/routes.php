<?php

$middleware = config('loopback-queue.middleware', []);

Route::middleware($middleware)->group(function () {
    Route::post('job/execute', [\MadBob\LaravelQueue\Loopback\Controllers\LoopbackQueueController::class, 'execute'])->name('job.execute');
});
