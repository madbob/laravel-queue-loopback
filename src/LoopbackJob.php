<?php

namespace MadBob\LaravelQueue\Loopback;

use Illuminate\Queue\Jobs\Job;
use Illuminate\Contracts\Queue\Job as JobContract;

class LoopbackJob extends Job implements JobContract
{
    protected $payload;

    public function __construct($container, $payload)
    {
        $this->container = $container;
        $this->payload = $payload;

        /*
            Warning: direct invocation of getInternalKey() on the QueueManager
            assumes that the Loopback connection is the default one.
            Probably here is required to identify the actual connection in use,
            and retrieve the configuration for it.
        */
        $internal_key = $this->payload()['key'] ?? '';
        if ($internal_key != app()->make('queue')->getInternalKey()) {
            throw new Exception('Invalid key', 1);
        }

        $delay = $this->payload()['delay'] ?? 0;
        if ($delay) {
            $time = \Carbon\Carbon::parse($delay);
            $delay = (int) ($time->timestamp - time());

            if ($delay > 0) {
                sleep($delay);
            }
        }
    }

    public function attempts()
    {
        return 1;
    }

    public function getJobId()
    {
        return '';
    }

    public function getRawBody()
    {
        return $this->payload;
    }
}
