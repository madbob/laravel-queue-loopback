<?php

namespace MadBob\LaravelQueue\Loopback\Connectors;

use Illuminate\Queue\Connectors\ConnectorInterface;

use MadBob\LaravelQueue\Loopback\LoopbackQueue;

class LoopbackConnector implements ConnectorInterface
{
    private $config;

    public function connect(array $config)
    {
        $this->config = $config;

        if (!isset($this->config['key']) || !filled($this->config['key'])) {
            $this->config['key'] = substr(config('app.key'), -10, 10);
        }

        return new LoopbackQueue($this->config['key'], $config['after_commit'] ?? false);
    }
}
