<?php

namespace MadBob\LaravelQueue\Loopback;

use Illuminate\Contracts\Queue\Queue as QueueContract;
use Illuminate\Queue\Queue;

class LoopbackQueue extends Queue implements QueueContract
{
    private $internal_key;

    public function __construct($key, $dispatchAfterCommit = false)
    {
        $this->internal_key = $key;
        $this->dispatchAfterCommit = $dispatchAfterCommit;
    }

    private function actualPush($postData)
    {
        $contentLength = strlen($postData);

        $endpoint = route('job.execute');
        $endpointParts = parse_url($endpoint);
        $endpointParts['path'] = $endpointParts['path'] ?? '/';
        $endpointParts['port'] = $endpointParts['port'] ?? $endpointParts['scheme'] === 'https' ? 443 : 80;

        $request = "POST {$endpointParts['path']} HTTP/1.1\r\n";
        $request .= "Host: {$endpointParts['host']}\r\n";
        $request .= "Content-Length: {$contentLength}\r\n";
        $request .= "Content-Type: application/json\r\n\r\n";
        $request .= $postData;

        $prefix = substr($endpoint, 0, 8) === 'https://' ? 'tls://' : '';
        $socket = fsockopen($prefix . $endpointParts['host'], $endpointParts['port']);
        fwrite($socket, $request);
        fclose($socket);
    }

    private function internalPayload($payload, $extras = [])
    {
        $postData = json_decode($payload);
        $postData->key = $this->internal_key;

        foreach($extras as $name => $value) {
            $postData->$name = $value;
        }

        return json_encode($postData);
    }

    public function push($job, $data = '', $queue = null)
    {
        return $this->enqueueUsing(
            $job,
            $this->createPayload($job, 'loopback', $data),
            $queue,
            null,
            function ($payload, $queue) {
                $postData = $this->internalPayload($payload);
                $this->actualPush($postData);
            }
        );
    }

    public function pushRaw($payload, $queue = null, array $options = [])
    {
        $payload = json_decode($payload);
        $payload->key = $this->internal_key;
        $payload = json_encode($payload);
        $this->actualPush($payload);
    }

    public function later($delay, $job, $data = '', $queue = null)
    {
        return $this->enqueueUsing(
            $job,
            $this->createPayload($job, 'loopback', $data),
            $queue,
            $delay,
            function ($payload, $queue) use ($delay) {
                $postData = $this->internalPayload($payload, ['delay' => $delay]);
                $this->actualPush($postData);
            }
        );
    }

    public function size($queue = null)
    {
        return 0;
    }

    public function pop($queue = null)
    {
        return null;
    }

    public function getInternalKey()
    {
        return $this->internal_key;
    }
}
