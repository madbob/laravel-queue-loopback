<?php

namespace MadBob\LaravelQueue\Loopback;

use Log;

use Illuminate\Support\ServiceProvider;

use MadBob\LaravelQueue\Loopback\Connectors\LoopbackConnector;

class LoopbackQueueServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/loopback-queue.php' => config_path('loopback-queue.php'),
        ], 'config');

        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        $this->app['queue']->addConnector('loopback', function () {
            return new LoopbackConnector();
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/loopback-queue.php', 'loopback-queue'
        );
    }
}
